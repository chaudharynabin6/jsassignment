const fs = require("fs");
const { resolve } = require("path");

const write = (filename, content) => {
  return new Promise((resolve, reject) => {
    fs.writeFile("./files/" + filename, content, (error, done) => {
      if (error) {
        reject(error);
      } else {
        resolve(done);
      }
    });
  });
};

const read = (filename) => {
  return new Promise((resolve, reject) => {
    fs.readFile("./files/" + filename, "UTF-8", (err, done) => {
      let data = err ? reject(err) : resolve(done);
    });
  });
};

const rename = (oldname, newname) => {
  return new Promise((resolve, reject) => {
    fs.rename(oldname, newname, (err, done) => {
      let log = err ? reject(err) : resolve(done);
    });
  });
};
module.exports = {
  write,
  read,
  rename,
};
