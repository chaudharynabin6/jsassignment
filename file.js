// import { write } from "./fileOperation";
// import { handle } from "./asyncAwait";
const { write, read, rename } = require("./fileOperation");
const { handle } = require("./asyncAwait");
const writeFile = async (
  filename = "kishor.txt",
  content = "hi i am kishor"
) => {
  let [data, error] = await handle(write(filename, content));

  if (error) throw new Error("file cannot be written", error);
  else {
    console.log(data);
  }
};

const readFile = async (filename) => {
  let [data, err] = await handle(read(filename));

  if (err) throw new Error("file cannot be read", err);
  else {
    console.log(data);
  }
};

const renameFile = async (oldPath, newPath) => {
  let [data, err] = await handle(rename(oldPath, newPath));
  if (err) return new Error("File cannot be renamed", err);

  console.log(data);
};

writeFile().catch((error) => {
  console.log(error);
});

readFile("kishor.txt").catch((err) => {
  console.log(err);
});

const readWriteRename = async () => {
  let [file, writeerr] = await handle(write("hari.txt", "hi i am hari"));
  if (writeerr) throw new Error("unable to write", writeerr);
  console.log(file);

  let [data, readerr] = await handle(read("hari.txt"));
  if (readerr) throw new Error("unable to read", readerr);
  console.log(data);

  let [renamed, renameErr] = await handle(
    rename("./files/hari.txt", "./files/other.txt")
  );
  if (renameErr) throw new Error("unable to rename", renameErr);
  console.log(renamed);
};

readWriteRename().catch((err) => console.log(err));
renameFile("./files/kishor.txt", "./files/nabin.txt").catch((err) =>
  console.log(err)
);

module.exports = {};
